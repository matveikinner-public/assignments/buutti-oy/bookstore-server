# ----------------------------------------------------------------------------------------------------------------------
#
#     ____             __             _____ __   
#    / __ \____  _____/ /_____  _____/ __(_) /__ 
#   / / / / __ \/ ___/ //_/ _ \/ ___/ /_/ / / _ \
#  / /_/ / /_/ / /__/ ,< /  __/ /  / __/ / /  __/
# /_____/\____/\___/_/|_|\___/_/  /_/ /_/_/\___/                                           
#
#
# For official documentation, see:
# https://docs.docker.com/engine/reference/builder/
#
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# BUILD ENVIRONMENT
#
# Description:
# Set up environment to create production ready Node / NestJS application
#
# For official documentation, see:
# https://docs.docker.com/develop/develop-images/multistage-build
#
# ----------------------------------------------------------------------------------------------------------------------

# Set base image
# See official available Node base images at Docker Hub https://hub.docker.com/_/node
FROM node:17-alpine AS builder

# Set current working directory
# See official /opt directory documentation at Linux Filesystem Hierarchy
# https://tldp.org/LDP/Linux-Filesystem-Hierarchy/html/opt.html
WORKDIR /opt/temp

# Copy all files from ./ to WORKDIR
COPY . .

# Install dependencies and devDependencies
RUN yarn install
RUN yarn cache clean --force

# Run script to create NestJS production bundle in the /opt/temp directory
RUN yarn run build

# ----------------------------------------------------------------------------------------------------------------------
# SERVER ENVIRONMENT
#
# Description:
# Set up environment to run production ready Node / NestJS application
#
# For official documentation, see:
# https://docs.docker.com/develop/develop-images/multistage-build
#
# ----------------------------------------------------------------------------------------------------------------------

# Set base image
# See official available Node base images at Docker Hub https://hub.docker.com/_/node
FROM node:17-alpine AS server

# Set image labels
LABEL MAINTAINER="Matvei Kinner"
LABEL MAINTAINER_EMAIL="hello@matveikinner.com"

# Set arguments which can be later overwritten higher in hierarchy with ex. Docker Compose, or .gitlab-ci.yml
ARG VERSION=0.0.1
ARG NODE_ENV=production
ARG PORT=3000

# Set environmental variables from arguments
ENV VERSION=$VERSION
ENV NODE_ENV=$NODE_ENV
ENV PORT=$PORT

# Install OS package upgrades, and update available package indexes
RUN apk upgrade --update

# Install TZData to set chosen Timezone
RUN apk add -U tzdata curl && \
  cp /usr/share/zoneinfo/Europe/Helsinki /etc/localtime && \
  apk del tzdata

# Install Tini Docker container tool to handle Kernel signals properly, see https://github.com/krallin/tini
RUN apk add --no-cache tini

# Clean OS package cache
RUN rm -rf /var/cache/apk/*

# Set current working directory
# See official /opt directory documentation at Linux Filesystem Hierarchy
# https://tldp.org/LDP/Linux-Filesystem-Hierarchy/html/opt.html
WORKDIR /opt/bookstore-server

# Copy package.json and package-lock.json to WORKDIR
COPY package.json yarn.lock ./

# Install exact dependecies (excluding devDependencies) from yarn.lock. NestJS (depending on the setup) requires
# several dependencies which are not possible to bundle with ex. Webpack due to depedency chains which can include C++
# For reference, see https://github.com/ZenSoftware/bundled-nest
RUN yarn install --frozen-lockfile --production=true

# Clean Yarn _cacache to free additional space
RUN yarn cache clean --force

# Copy NestJS bundle from builder step
COPY --from=builder /opt/temp/dist ./dist

# Execute Tini as Docker entrypoint
ENTRYPOINT ["/sbin/tini", "--"]

# Run NestJS server main executable file
CMD [ "node", "dist/src/main" ]

# Run healthcheck to see that the Node server responds
HEALTHCHECK --interval=30s --timeout=5s \
  CMD curl -f http://localhost:$PORT/api || exit 1
