# Bookstore Server

## Assignment

> Implement a simple single-page web application which manages a collection of books. Your
> web application should have a UI consisting of a single HTML web page, and a simple
> backend. The backend provides a REST API with which the UI communicates. We want the
> application to have the following features:
>
> 1.  When the web page is loaded, it fetches all the books to a list. The title and the
>     author of each book are displayed in the list.
> 2.  When a book in the list is clicked, it is selected and its author, title and description are
>     displayed in a form next to the list.
> 3.  By inputting data to the form and pressing a button labelled “save new”, the user can
>     add new books to the collection.
> 4.  By editing the form data of an existing book and pressing a button labelled “save”,
>     the user can update the data of the book in the collection.
> 5.  There is also a delete button that can be used to delete a selected book from the
>     collection.
> 6.  All the changes that user has made to the collection must be preserved on a page
>     reload.
> 7.  The application (front and backend) can be started with a single command in terminal

## Installation

### Prerequisites

**Important!** You will require both [Bookstore UI](https://gitlab.com/matveikinner-public/assignments/buutti-oy/bookstore-ui) and [Bookstore Server](https://gitlab.com/matveikinner-public/assignments/buutti-oy/bookstore-server) repositories from GitLab in the same parent directory to be able to launch / orchestrate all Docker containers required to run the application

- Bookstore UI launches on localhost port 80
- Bookstore Server launches on localhost port 3000

### Scripts

#### Makefile

To launch application containers with tiny shell CLI you can use Makefile `make` commands

To launch / orchestrate all Docker containers run

    make docker

Then select

1.  Option 3 to "Compose"
2.  Option 1 to "Up"

#### Docker

To launch / orchestrate all Docker containers run

    docker-compose -f docker-compose.development.yml up

#### Yarn

To install dependencies run

    yarn install

To launch application with Webpack run

    yarn start:<dev | prod>

To build application with Webpack run

    yarn build:<dev | prod>

## Solution

### Features

- Books API to GET, POST, PUT and DELETE books (where GET supports pagination and sorting though not it use)
- PostgreSQL as a database with TypeORM
- Incoming request validation
- Top-notch (as it comes to performance) custom logger with Pino
- Consideration for API versioning since latest NestJS v8 supports it natively
- OpenAPI 3.0 specs with Swagger

### Learnings

- Pagination support with TypeORM

### Caveats / Improvements

- No cache solution ex. Redis
- No tests
- No deployment to Azure (can be done though PostgreSQL costs few bucks to provision). Also no GitLab CI / CD pipeline

## Documentation

### Conventions

#### Commits

The [Commitlint](https://commitlint.js.org/#/) enforces best practices together with [Commitlint Jira rules](https://github.com/Gherciu/commitlint-jira). The commit message issue status must comply with [default Jira statuses](https://confluence.atlassian.com/adminjiracloud/issue-statuses-priorities-and-resolutions-973500867.html) which follow task ID (from Jira board or as running repository prefix and enumeration), task description (as abbreviation), and commit message which must be in present tense. These must all be in capital letters.

Format

    git commit -m "[<ISSUE-STATUS>]<TASK-ID>: [<TASK-DESCRIPTION>] <message>"

As an example

    git commit -m "[CLOSED]TASK-1: [CONF] Create .gitlab-ci.yml production branch CI / CD pipeline

The exception to this are situations when there are merge conflicts which require resolution in main branches (see **Branching Strategy**).

Format

    git commit -m "[RESOLVED]<TASK-ID>: [MERGE] <message>"

As an example

    git commit -m "[RESOLVED]TASK-1: [MERGE] Merge TASK-1 to development"

##### Task descriptions

- [CONF] Informs that the ticket task description concerns configuration
- [FEAT] Informs that the ticket task description concerns feature
- [UPDT] Informs that the ticket task description concerns update to an existing feature / capability
- [FIX] Informs that the ticket task description concerns fix to a particular known issue / bug

_The option to include custom commit rules to include all possible scenarios is up to later consideration_

#### Branching Strategy

##### Overview

There are three main branches — development, staging, and production. These branches contain CI / CD pipeline which builds to corresponding domain / FQDN ex. https://\<branch>.\<domain>.\<domain-extension>

```mermaid
graph LR
T(Ticket) --> D(Development)
T(Ticket) --> F(Feature)
F --> D
D --> M(Master)
M --> S(Staging)
S --> P(Production)
```

##### Ticket

The ticket branch can branch out from either development branch or feature branch. Once complete the ticket branch merges always back to its parent branch. The only exceptions are unconventional situations where there is requirement to ex. cherry pick a particular commit higher in the tree to patch issues which require immediate attention. The ticket branch name derives from Jira board in use.

##### Feature

The feature branch can branch out only from the development branch. Once complete the feature branch merges always back to its parent branch. The feature branch name has "feature-" prefix which follows brief feature description ex. feature-password-reset.

##### Development

The development branch contains latest code.

##### Master

The master branch must contain at all times ready code ready for release to staging.

##### Staging

The staging branch must contain at all time ready code ready for release to production. Releases to this branch require appropriate version tag as a release candidate postfix and message.

Format

    git tag -a <semver>_rc<number> -m "<message>"

As an example

    git tag -a 1.0.0_rc1 -m "Release 1.0.0~rc1 to staging"

##### Production

The production branch contains latest official release. Releases to this branch require appropriate version tag and message.

Format

    git tag -a <semver> -m "<message>"

As an example

    git tag -a 1.0.0 -m "Release 1.0.0 to production"

#### Versions

The [Semantic Versioning](https://semver.org/) applies.
