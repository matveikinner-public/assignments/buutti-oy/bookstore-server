enum ConfigEnum {
  SHARED = "SHARED",
  DATABASE = "DATABASE",
  VALIDATION = "VALIDATION",
  LOGGER = "LOGGER",
}

export default ConfigEnum;
