import { registerAs } from "@nestjs/config";
import ConfigEnum from "./config.enum";

export interface SharedConfigOptions {
  port: number;
  name: string;
  version: string;
}

export default registerAs(
  ConfigEnum.SHARED,
  (): SharedConfigOptions => ({
    port: parseInt(process.env.PORT) || 3000,
    name: process.env.NODE_ENV ? `Bookstore API - ${process.env.NODE_ENV.toUpperCase()}` : "Bookstore API",
    version: process.env.VERSION || "0.0.1",
  })
);
