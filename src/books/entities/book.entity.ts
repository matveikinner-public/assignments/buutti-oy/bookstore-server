import { BaseEntity, PrimaryGeneratedColumn, Column, Entity } from "typeorm";
import IBook from "../interfaces/book.interface";

@Entity()
export class BookEntity extends BaseEntity implements IBook {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  author: string;

  @Column()
  title: string;

  @Column()
  description: string;
}
