import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import PagedRequestDto from "@shared/dtos/paged-request.dto";
import { from, map, mergeMap } from "rxjs";
import { Repository, Like } from "typeorm";
import BookDto from "./dtos/create-book.dto";
import UpdateBookDto from "./dtos/update-book.dto";
import { BookEntity } from "./entities/book.entity";

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(BookEntity)
    private booksRepository: Repository<BookEntity>
  ) {}

  /**
   * Lazy solution to seed database on init
   */
  onModuleInit() {
    void this.booksRepository
      .create({
        author: "Roald Dahl",
        title: "Fantastic Mr. Fox",
        description:
          "The main character of Fantastic Mr. Fox is an extremely clever anthropomorphized fox named Mr. Fox. He lives with his wife and four little foxes. In order to feed his family, he steals food from the cruel, brutish farmers named Boggis, Bunce, and Bean every night.",
      })
      .save();
  }

  getBooks(query: PagedRequestDto) {
    return from(
      this.booksRepository.findAndCount({
        where: { [query.sortBy || "author"]: Like("%" + "" + "%") },
        order: { [query.sortBy || "author"]: query.sortOrder },
        take: query.size || 20,
        skip: query.skip || 0,
      })
    ).pipe(map(([books, total]) => ({ books, total })));
  }

  createBook(book: BookDto) {
    return from(this.booksRepository.create(book).save());
  }

  updateBook(id: string, body: UpdateBookDto) {
    return from(this.booksRepository.findOneOrFail(id)).pipe(
      mergeMap((book) => from(this.booksRepository.save({ ...book, ...body })))
    );
  }

  deleteBook(id: string) {
    return from(this.booksRepository.delete(id)).pipe(map((result) => (result.affected > 0 ? "Success" : "Failure")));
  }
}
