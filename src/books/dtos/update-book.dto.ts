import { PartialType } from "@nestjs/swagger";
import CreateBookDto from "./create-book.dto";

export default class UpdateBookDto extends PartialType(CreateBookDto) {}
