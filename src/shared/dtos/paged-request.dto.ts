import { IsEnum, IsNumber, IsOptional, IsString } from "class-validator";
import { Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

enum SortOrderEnum {
  ASCENDING = "ASC",
  DESCENDING = "DESC",
}

export default class PagedRequestDto {
  @ApiProperty({
    type: Number,
    description: "The result size from database",
    example: 25,
  })
  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  readonly size: number;

  @ApiProperty({
    type: Number,
    description: "The results to skip from database",
    example: 0,
  })
  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  readonly skip: number;

  @ApiProperty({
    type: String,
    description: "Sort criteria when getting entities from database",
    example: 0,
  })
  @IsOptional()
  @IsString()
  readonly sortBy: string;

  @ApiProperty({
    type: SortOrderEnum,
    description: "Sort order criteria when getting entities from database",
    example: 0,
  })
  @IsOptional()
  @IsEnum(SortOrderEnum)
  readonly sortOrder: SortOrderEnum;
}
