import { ApiProperty } from "@nestjs/swagger";
import { IsUUID } from "class-validator";

export default class UuidDto {
  @ApiProperty({
    type: String,
    description: "A universally unique identifier (UUID)",
    example: 0,
  })
  @IsUUID()
  id: string;
}
