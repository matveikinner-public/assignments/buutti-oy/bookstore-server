import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from "@nestjs/common";
import { TypeORMError } from "typeorm";
import { Request, Response } from "express";

@Catch(TypeORMError)
export default class TypeORMExceptionFilter implements ExceptionFilter {
  catch(ex: TypeORMError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const message = ex.message;
    const error = ex.name;

    return response.status(HttpStatus.BAD_REQUEST).json({
      statusCode: HttpStatus.BAD_REQUEST,
      error,
      message,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
