import { DynamicModule, Global, Inject, MiddlewareConsumer, Module, Provider } from "@nestjs/common";
import AsyncParams from "./interfaces/asyncParams.interface";
import Params from "./interfaces/params.interface";
import { DEFAULT_ROUTES, PINO_LOGGER_OPTIONS } from "./logger.constants";
import { LoggerService } from "./logger.service";
import { Request, Response, NextFunction } from "express";

@Global()
@Module({
  providers: [LoggerService],
  exports: [LoggerService],
})
export class LoggerModule {
  constructor(
    private readonly logger: LoggerService,
    @Inject(PINO_LOGGER_OPTIONS) private readonly params?: Params | undefined
  ) {}

  static forRoot(params?: Params | undefined): DynamicModule {
    const paramsProvider: Provider<Params> = {
      provide: "PINO_LOGGER_OPTIONS",
      useValue: params || {},
    };
    return {
      module: LoggerModule,
      providers: [paramsProvider, LoggerService],
      exports: [LoggerService],
    };
  }

  static forRootAsync(params?: AsyncParams): DynamicModule {
    const paramsProvider: Provider<Params | Promise<Params>> = {
      provide: "PINO_LOGGER_OPTIONS",
      useFactory: params.useFactory,
      inject: params.inject,
    };
    return {
      module: LoggerModule,
      imports: params.imports,
      providers: [paramsProvider, ...(params.providers || []), LoggerService],
      exports: [LoggerService],
    };
  }

  configure(consumer: MiddlewareConsumer) {
    if (this.params) {
      consumer.apply(this.loggerMiddleware).forRoutes(...(this.params.forRoutes || DEFAULT_ROUTES));
    } else consumer.apply(this.loggerMiddleware).forRoutes(...DEFAULT_ROUTES);
  }

  loggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
    this.logger.middleware("Incoming request", "LoggerMiddleware", req, res);
    next();
  };
}
