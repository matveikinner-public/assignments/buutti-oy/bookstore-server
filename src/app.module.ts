import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TransformInterceptor } from "@shared/interceptors/transform.interceptor";
import { LoggerModule } from "@shared/logger/logger.module";
import { BooksModule } from "./books/books.module";
import ConfigEnum from "./configs/config.enum";
import { sharedConfig, databaseConfig } from "./configs/index";

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [sharedConfig, databaseConfig],
      isGlobal: true,
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => await configService.get(ConfigEnum.LOGGER),
      inject: [ConfigService],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => configService.get(ConfigEnum.DATABASE),
      inject: [ConfigService],
    }),
    BooksModule,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
  ],
})
export class AppModule {}
